/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MenuApi;

/**
 *
 * @author robb
 */
public interface MenuListener {
    
    void executeMenuAction(String menuName, int id);
}
