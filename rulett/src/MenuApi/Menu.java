/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MenuApi;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author robb
 */
public class Menu {

    private ArrayList<Menu> submenus;
    private Menu parentMenu;
    private String name;
    private MenuListener ml;

    private boolean returnAfterAction;
    private Menu defaultMenuAfterAction;

    private String colorCode = "\u001B[30m";

    private static String wrongMenuPoint = "Kérlek a fentiek közül válassz egy számot!";
    private static String unsupportedAction = "..unsupported action yet..";

    private static int idCount = 0;
    private int id;

    public Menu() {
        submenus = new ArrayList<>();
        returnAfterAction = false;
        id = idCount;
        idCount++;
    }
    

    public Menu(String name, Menu parentMenu) {
        this.parentMenu = parentMenu;
        this.name = name;
        submenus = new ArrayList<>();
        parentMenu.getSubmenus().add(this);
        returnAfterAction = parentMenu.getReturnAfterAction();
        id = idCount;
        idCount++;
        this.ml = parentMenu.ml;
    }

    public Menu(String name) {
        this.name = name;
        submenus = new ArrayList<>();
        returnAfterAction = false;
        id = idCount;
        idCount++;
    }
    
    public Menu(String name, int id) {
        this.name = name;
        submenus = new ArrayList<>();
        returnAfterAction = false;
        if (isLegitIdOfRoot(id)) {
            this.id = id;
        } else {
            id = idCount;
        idCount++;
        }
    }

    public Menu(String name, Menu parentMenu, int id) {
        this.parentMenu = parentMenu;
        this.name = name;
        submenus = new ArrayList<>();
        parentMenu.getSubmenus().add(this);
        returnAfterAction = parentMenu.getReturnAfterAction();
        if (isLegitIdOfRoot(id)) {
            this.id = id;
        } else {
            id = idCount;
        idCount++;
        }
    }

    public Menu(String name, Menu parentMenu, MenuListener ml) {
        this.parentMenu = parentMenu;
        this.name = name;
        submenus = new ArrayList<>();
        parentMenu.getSubmenus().add(this);
        this.ml = ml;
        returnAfterAction = parentMenu.getReturnAfterAction();
        id = idCount;
        idCount++;
    }

    public Menu(String name, MenuListener ml) {
        returnAfterAction = false;
        this.name = name;
        submenus = new ArrayList<>();
        this.ml = ml;
        id = idCount;
        idCount++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Menu> getSubmenus() {
        return submenus;
    }

    public void setSubmenus(ArrayList<Menu> submenus) {
        this.submenus = submenus;
    }

    public Menu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(Menu parentMenu) {
        this.parentMenu = parentMenu;
    }

    public MenuListener getListener() {
        return ml;
    }

    public void setListener(MenuListener ml) {
        this.ml = ml;
    }

    public boolean getReturnAfterAction() {
        return returnAfterAction;
    }

    public Menu getDefaultMenuAfterAction() {
        return defaultMenuAfterAction;
    }

    /**
     * Action menüpont választása után melyik menübe térjen vissza. A
     * hierarchiában lefelé öröklődik az összes submenüre.
     *
     * @param defaultMenuAfterAction
     */
    public void setDefaultMenuAfterAction(Menu defaultMenuAfterAction) {
        changeAllSubmenusDefaultMenu(defaultMenuAfterAction);
        changeAllSubmenusDefaultReturn(false);
    }

    /**
     * Csak erre a menüre vonatkozóan állítja be a visszatérési menüpontot.
     *
     * @param defaultMenuAfterAction
     */
    public void setDefaultMenuAfterActionJustThis(Menu defaultMenuAfterAction) {
        this.defaultMenuAfterAction = defaultMenuAfterAction;
        returnAfterAction = false;
    }

    public static String getWrongMenuPoint() {
        return wrongMenuPoint;
    }

    /**
     * Milyen üzenet jelenjen meg rossz bevitelkor. Minden menüobjektumra
     * kiterjed.
     *
     * @param wrongMenuPoint
     */
    public static void setWrongMenuPoint(String wrongMenuPoint) {
        Menu.wrongMenuPoint = wrongMenuPoint;
    }

    public static String getUnsupportedAction() {
        return unsupportedAction;
    }

    /**
     * Milyen üzenet jelenjen meg, ha még nincs MenuListener rendelve a
     * végrehajtható menüponthoz. Minden menüobjektumra kiterjed.
     *
     * @param unsupportedAction
     */
    public static void setUnsupportedAction(String unsupportedAction) {
        Menu.unsupportedAction = unsupportedAction;
    }

    /**
     * Ha egy olyan menüpontot választunk, ami végrehajtható, akkor a lefutás
     * után kilépjen-e a program, vagy visszaugorjon az előző menübe.
     * Alapértelmezés szerint a navigálás folytatódik. A hierarchiában lefelé
     * öröklődik az összes submenüre.
     *
     * @param returnAfterAction
     */
    public void setReturnAfterAction(boolean returnAfterAction) {
        changeAllSubmenusDefaultReturn(returnAfterAction);
    }

    /**
     * Csak erre a menüpontra vonatkozóan állítja be a visszatérést.
     *
     * @param returnAfterAction
     */
    public void setReturnAfterActionJustThis(boolean returnAfterAction) {
        this.returnAfterAction = returnAfterAction;
    }

    /**
     * Megjeleníti a menüt és megvalósítja a navigációt a submenük között.
     *
     * q - kilépés action nélkül c - az aktuális menü színének megváltoztatása
     * egy random értékre
     */
    public void displayMenu() {
        printText(name.toUpperCase() + "\n");
        for (int i = 0; i < name.length(); i++) {
            printText("-");
        }
        System.out.println("");

        if (!submenus.isEmpty()) {
            int i = 1;
            for (Menu submenu : submenus) {
                printText(i + " - " + submenu.getName());
                if (!submenu.getSubmenus().isEmpty()) {
                    printText(" >");
                }
                System.out.println();
                i++;
            }
            if (this.parentMenu != null) {
                printText(i + " - Vissza" + "\n");
            }

            Scanner sc = new Scanner(System.in);
            String str = sc.nextLine();
            while (!isLegitNumber(str)) {
                if (str.equals("q")) {
                    return;
                }
                if (str.equals("c")) {
                    newRandomColor();
                    displayMenu();
                    return;
                }
                System.out.println("\u001B[31m" + wrongMenuPoint + "\u001B[0m");
                str = sc.nextLine();
            }

            int idx = Integer.parseInt(str);

            if (idx > submenus.size()) {
                parentMenu.displayMenu();
                return;
            }

            if (submenus.get(idx - 1).getSubmenus().isEmpty() && submenus.get(idx - 1).getListener() != null) {
                submenus.get(idx - 1).getListener().executeMenuAction(submenus.get(idx - 1).getName(), submenus.get(idx - 1).getId());
                //System.out.println(id);
                if (!returnAfterAction) {
                    if (defaultMenuAfterAction != null) {
                        defaultMenuAfterAction.displayMenu();
                    } else {
                        getParentMenu().displayMenu();
                    }
                }
            } else {
                submenus.get(idx - 1).displayMenu();
            }

        } else {
            printText(unsupportedAction + "\n");
            if (!returnAfterAction) {
                if (defaultMenuAfterAction != null) {
                    defaultMenuAfterAction.displayMenu();
                } else {
                    getParentMenu().displayMenu();
                }
            }
        }

    }

    public void addSubmenu(Menu m) {
        this.submenus.add(m);
        m.setParentMenu(this);
    }

    private boolean isLegitNumber(String str) {
        int num = 0;
        try {
            num = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        if (this.parentMenu != null) {
            return num > 0 && num <= submenus.size() + 1;
        } else {
            return num > 0 && num <= submenus.size();
        }
    }

    public boolean hasListener() {
        return this.ml != null;
    }

    /**
     * Megváltosztatja a menü színét alapértelmezett feketéről a @Color enum
     * osztály lehetőségeinek megfelelően. Új almenük beszúrásakor ez az érték
     * öröklődik.
     *
     * @param color egy érték a Menu.Color - ból
     */
    public void setColor(Color color) {
        switch (color) {
            case WHITE:
                colorCode = "\u001B[37m";
                break;
            case BLUE:
                colorCode = "\u001B[34m";
                break;
            case GREEN:
                colorCode = "\u001B[32m";
                break;
            case RED:
                colorCode = "\u001B[31m";
                break;
            case BLACK:
                colorCode = "\u001B[30m";
                break;
            case PURPLE:
                colorCode = "\u001B[35m";
                break;
            case CYAN:
                colorCode = "\u001B[36m";

        }

    }

    /**
     * Ennek a menünek és az összes almenüjének a hierarchiában beállítja a
     * színt.
     *
     * @param color
     */
    public void setColorAllDown(Color color) {
        setColor(color);
        submenus.forEach((submenu) -> {
            submenu.setColorAllDown(color);
        });
    }

    private void printText(String text) {
        System.out.print(colorCode + text + "\u001B[0m");
    }

    /**
     * Ad egy új random színt a menünek.
     */
    public void newRandomColor() {
        String str = colorCode;
        while (str.equals(colorCode)) {
            str = "\u001B[3" + (int) (Math.random() * 8 + 1) + "m";
        }
        colorCode = str;
    }

    /**
     * A menü lehetséges színei
     */
    public enum Color {
        WHITE, BLUE, GREEN, RED, BLACK, PURPLE, CYAN;
    }

    /**
     * Menürendszer létrehozása fájlból.
     *
     *
     *
     * @param path a fájl elérési útja a hierarchiát \n és \t szemlélteti
     * @param ml MenuListener
     * @return
     * @throws java.io.FileNotFoundException
     */
    public int constructFromFile(String path, MenuListener ml) throws NullPointerException, FileNotFoundException {
        Scanner sc;

        sc = new Scanner(new File(path));
        if (sc == null) {
            throw new FileNotFoundException("");
        }

        if (ml == null) {
            throw new NullPointerException("A MenuListener nem lehet null!");
        }
        /*
        name = sc.nextLine();
        if (name.contains("\t")) {
            //rossz formátum
            return -2;
        }*/
        Menu parent = this;
        int i = 0;
        Menu m = this;
        while (sc.hasNextLine()) {

            String[] str = sc.nextLine().split("\t");
            if (str.length < 1) {
                //rossz formátum
                return -2;
            }
            //ha szomsédos menüpont
            if (str.length == i + 1) {
                m = new Menu(str[i], parent, ml);
            }
            //ha almenü lesz
            if (str.length > i + 1) {
                parent = m;
                i++;
                m = new Menu(str[i], parent, ml);

            }
            //ha feljebb van a hierarchiában
            if (str.length < i + 1) {
                parent = parent.getparentRek((i + 1) - (str.length - 1));
                m = new Menu(str[str.length - 1], parent);

                i = str.length - 1;
            }

        }

        return 0;
    }

    /**
     * K - adik mélységig visszakeresi a menü parentmenüjét.
     *
     * @param k a keresett parentmenü távolsága a hierarchiában
     * @return
     */
    public Menu getparentRek(int k) {
        if (k <= 1) {
            return this;
        } else {
            if (parentMenu != null) {
                return this.getParentMenu().getparentRek(k - 1);
            } else {
                return this;
            }
        }
    }

    private void changeAllSubmenusDefaultReturn(boolean value) {
        returnAfterAction = value;
        submenus.forEach((submenu) -> {
            submenu.changeAllSubmenusDefaultReturn(value);
        });

    }

    private void changeAllSubmenusDefaultMenu(Menu menu) {
        defaultMenuAfterAction = menu;
        submenus.forEach((submenu) -> {
            submenu.changeAllSubmenusDefaultMenu(menu);
        });

    }

    public int getIdByName(String namestr) {
        if (this.name.equals(namestr)) {
            return this.id;
        } else {
            for (Menu submenu : submenus) {
                int k = submenu.getIdByName(namestr);
                if (k != -1) {
                    return k;
                }
            }
        }
        return -1;
    }

    public boolean isLegitIdOfRoot(int id) {
        return getRoot().isLegitId(id);
    }

    public boolean isLegitId(int id) {
        if (this.id == id) {
            return false;
        } else {
            for (Menu submenu : submenus) {
                if (!submenu.isLegitId(id)) {
                    return false;
                }
            }
        }
        return true;
    }

    public Menu getRoot() {
        Menu ret = this;
        while (ret.parentMenu != null) {
            ret = ret.parentMenu;
        }
        return ret;
    }
    
    public void printMenu(int k){
        for (int i = 0; i < k; i++) {
            System.out.print("\t");
        }
        System.out.print(this.id+": "+this.name+"\n");
        k++;
        for (Menu submenu : submenus) {
            submenu.printMenu(k);
        }
    }

}
