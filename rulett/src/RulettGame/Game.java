package RulettGame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import MenuApi.Menu;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author robb
 */
public class Game {

    private HashMap<String, Position> placeablePositions;
    private long account = 10_000;
    private Bet actualBet = new Bet();
    private int number = -1;
    private final ArrayList<Player> players = new ArrayList<>();
    private long minimumBet = 1;
    private long maximumBet = 500;
    private boolean playerListInited = false;

    public Game() {
        beginGame();
    }

    public static void main(String[] args) {

        Game g = new Game();
        
        
        
        
        
    }
    
    
    
    
    
    public final void beginGame(){
        Scanner sc = new Scanner(System.in);
        String str = "";
        System.out.println("Üdvözöllek a Rulett játékban!");
        System.out.println("==Játékbeállítások==");
        System.out.println("Mekkora tőkével kezdjenek a játékosok?");
        while (str.equals("")) {
            str = sc.nextLine();
            try {
                account = Long.parseLong(str);
            } catch (NumberFormatException e) {
                System.out.println("Ez nem szám! Kérlek egy számot adj meg!");
                str = "";
            }
            if (!str.equals("") && (account < 100 || account > 1_000_000_000)) {
                System.out.println("A szám 100 és 1_000_000_000 közötti lehet! Próbáld meg újra!");
                str = "";
            }
        }
        str = "";
        System.out.println("Mekkora legyen az alaptét az asztalnál?");
        while (str.equals("")) {
            str = sc.nextLine();
            try {
                minimumBet = Long.parseLong(str);
            } catch (NumberFormatException e) {
                System.out.println("Ez nem szám! Kérlek egy számot adj meg!");
                str = "";
            }
            if (!str.equals("") && (minimumBet < 1 || minimumBet > account / 2)) {
                System.out.println("Az alaptét 1 és a játékosok alaptőkéjének fele közötti érték lehet! Próbáld meg újra!");
                str = "";
            }
        }
        str = "";
        System.out.println("Mekkora legyen a maximum tét az asztalnál?");
        while (str.equals("")) {
            str = sc.nextLine();
            try {
                maximumBet = Long.parseLong(str);
            } catch (NumberFormatException e) {
                System.out.println("Ez nem szám! Kérlek egy számot adj meg!");
                str = "";
            }
            if (!str.equals("") && (maximumBet <= minimumBet || maximumBet > 100_000_000_000l)) {
                System.out.println("A maximum tét az alaptét és százmilliárd közötti érték lehet! Próbáld meg újra!");
                str = "";
            }
        }

        Menu main = new Menu("Mit szeretnél csinálni?", (String menuName, int id) -> {
            if (menuName.equals("Játék botokkal vagy más játékosokkal")) {
                
                String str1 = "";
                int cnt = 0;
                System.out.println("Hány körös legyen a játék?");
                while (str1.equals("")) {
                    str1 = sc.nextLine();
                    try {
                        cnt = Integer.parseInt(str1);
                    } catch (NumberFormatException e) {
                        System.out.println("Ez nem szám! Kérlek egy számot adj meg!");
                        str1 = "";
                    }
                    if (!str1.equals("") && (cnt < 1 || cnt > 10_000)) {
                        System.out.println("A körök száma 1 és 10_000 között lehet! Próbáld meg újra!");
                        str1 = "";
                    }
                }
                
                
                doSimulation(cnt, false);
            }
            if (menuName.equals("Szimuláció")) {
                String str1 = "";
                int cnt = 0;
                System.out.println("Hány körös legyen a szimuláció?");
                while (str1.equals("")) {
                    str1 = sc.nextLine();
                    try {
                        cnt = Integer.parseInt(str1);
                    } catch (NumberFormatException e) {
                        System.out.println("Ez nem szám! Kérlek egy számot adj meg!");
                        str1 = "";
                    }
                    if (!str1.equals("") && (cnt < 1 || cnt > 1_000_000_000)) {
                        System.out.println("A körök száma 1 és 1_000_000_000 közötti lehet! Próbáld meg újra!");
                        str1 = "";
                    }
                }
                doSimulation(cnt, true);
            }
            if (menuName.equals("Gyors szimuláció (a részeredmények kiírása nélkül)")) {
                String str2 = "";
                int cnt = 0;
                System.out.println("Hány körös legyen a szimuláció?");
                while (str2.equals("")) {
                    str2 = sc.nextLine();
                    try {
                        cnt = Integer.parseInt(str2);
                    } catch (NumberFormatException e) {
                        System.out.println("Ez nem szám! Kérlek egy számot adj meg!");
                        str2 = "";
                    }
                    if (!str2.equals("") && (cnt < 1 || cnt > 1_000_000_000)) {
                        System.out.println("A körök száma 1 és 1_000_000_000 közötti lehet! Próbáld meg újra!");
                        str2 = "";
                    }
                }
                doQuickSimulation(cnt);
            }
        });

        Menu s1 = new Menu("Játék botokkal vagy más játékosokkal", main);
        Menu s2 = new Menu("Szimuláció", main);
        Menu s3 = new Menu("Gyors szimuláció (a részeredmények kiírása nélkül)", main);

        main.setColor(Menu.Color.BLUE);
        main.setReturnAfterAction(true);
        main.displayMenu();

        Menu exit = new Menu("Akarsz még játszani?", (String menuName, int id) -> {
            if (menuName.equals("Igen!")) {
                Game game = new Game();
            }
            if (menuName.equals("Nem, elég volt...")) {
                System.out.println("Viszlát!");
            }
        });

        Menu e1 = new Menu("Igen!", exit);
        Menu e2 = new Menu("Nem, elég volt...", exit);
        exit.setColor(Menu.Color.BLUE);
        exit.setReturnAfterAction(true);
        exit.displayMenu();

    }

    public HashMap<String, Position> getPlaceablePositions() {
        return placeablePositions;
    }

    public long getAccount() {
        return account;
    }

    public void setAccount(long account) {
        this.account = account;
    }

    public Bet getActualBet() {
        return actualBet;
    }

    public void setActualBet(Bet actualBet) {
        this.actualBet = actualBet;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isPlayerListInited() {
        return playerListInited;
    }

    public void setPlayerListInited(boolean playerListInited) {
        this.playerListInited = playerListInited;
    }

    public void initPlayers(boolean simulation) {

        if (playerListInited) {
        } else {

            Menu main = new Menu("Válassz játékost!", (String menuName, int id) -> {
                if (menuName.equals("Martin")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "Martin"));
                }
                if (menuName.equals("CareFulMartin")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "CareFulMartin"));
                }
                if (menuName.equals("AgressiveMartin")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "AgressiveMartin"));
                }
                if (menuName.equals("RandomBéla")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "RandomBéla"));
                }
                if (menuName.equals("RareNumberGéza")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "RareNumberGéza"));
                }
                if (menuName.equals("RarestColorZoltán")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "RarestColorZoltán"));
                }
                if (menuName.equals("PatientMartin")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "PatientMartin"));
                }
                if (menuName.equals("ProgressiveHarry")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "ProgressiveHarry"));
                }
                if (menuName.equals("Fibonacci")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "Fibonacci"));
                }
                if (menuName.equals("1326Karcsi")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "1326Karcsi"));
                }
                if (menuName.equals("FiveRarest")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "FiveRarest"));
                }
                if (menuName.equals("MultiMartin")) {
                    addPlayer(new Player(minimumBet, maximumBet, account, "MultiMartin"));
                }
                if (menuName.equals("Elég lesz ennyi.")) {
                    setPlayerListInited(true);
                }
                if (menuName.equals("Játékos infók")) {
                    Player.printPlayerInfos();
                }
                if (menuName.equals("Human")) {
                    Scanner sc = new Scanner(System.in);
                    System.out.println("Mi legyen a játékos neve?");
                    String name = sc.nextLine();
                    while (name.equals("")) {
                        System.out.println("Adj meg egy nevet, kérlek!");
                        name = sc.nextLine();
                    }
                    boolean uniqe = false;
                    while (!uniqe){
                    uniqe = true;    
                    for (Player player : players) {
                        if (player.getName().equals(name)){
                            uniqe = false;
                        }
                    }
                    if (!uniqe){
                        System.out.println("Már van ilyen név! Adj meg egy másikat, légyszíves!");
                        name = sc.nextLine();
                    }
                    }
                    addPlayer(new Player(name, minimumBet, maximumBet, account, "Human"));
                    
                }
            });
            if (!simulation) {
                Menu s1 = new Menu("Human", main);
            }
            Menu s2 = new Menu("Martin", main);
            Menu s3 = new Menu("CareFulMartin", main);
            Menu s4 = new Menu("AgressiveMartin", main);
            Menu s5 = new Menu("RandomBéla", main);
            Menu s6 = new Menu("RareNumberGéza", main);
            Menu s7 = new Menu("RarestColorZoltán", main);
            Menu s8 = new Menu("PatientMartin", main);
            Menu s9 = new Menu("ProgressiveHarry", main);
            Menu s10 = new Menu("Fibonacci", main);
            Menu s11 = new Menu("1326Karcsi", main);
            Menu s12 = new Menu("FiveRarest", main);
            Menu s13 = new Menu("MultiMartin", main);
            Menu s14 = new Menu("Játékos infók", main);
            if (players.size() > 0) {
                Menu s20 = new Menu("Elég lesz ennyi.", main);
            }
            main.setReturnAfterAction(true);
            main.setColor(Menu.Color.PURPLE);
            main.displayMenu();

            initPlayers(simulation);
        }

    }

    public void doSimulation(int count, boolean simulation) {

        RulettTable table = new RulettTable("European");
        this.placeablePositions = table.getPlaceablePositions();
        initPlayers(simulation);
        System.out.println(this.players.size() + " játékos vesz részt a szimulációban.");

        for (int i = 1; i <= count; i++) {
            if (i > 1) {
                System.out.println("#######");
            }
            System.out.println("Kör: " + i + ".");

            boolean zeroPlayer = true;
            for (Player player : players) {
                if (player.isPlaying()) {
                    zeroPlayer = false;
                    break;
                }
            }

            if (zeroPlayer) {
                System.out.println("A játék véget ért, elfogytak a játékosok.");
                break;
            }

            players.stream().filter((player) -> (player.isPlaying())).forEachOrdered((player) -> {
                ArrayList<Bet> b = player.getNextBet(table);
                if (b != null && !b.isEmpty()) {
                    if (b.get(0).getMoney() != 0) {
                        table.placeBet(b);
                        for (Bet bet : b) {
                            System.out.println(player.getName() + " játékos " + bet.getMoney() + "-t tett erre: " + bet.getPos().getNameId());
                        }
                        
                    } else {
                        System.out.println(player.getName() + " játékos nem tett ebben a körben.");
                    }
                } else {
                    System.out.println(player.getName() + " kiszállt a játékból.");
                }
            });
            System.out.println();
            number = table.spin();
            System.out.print("A kipörgetett szám: ");
            if (number == 0) {
                System.out.print("\u001B[32m");
            } else if (table.getPlaceablePositions().get("Red").getNumbers().contains(number)) {
                System.out.print("\u001B[31m");
            }
            System.out.print(number);
            System.out.print("\n");
            System.out.print("\u001B[0m");
            
            System.out.println();
            players.stream().filter((player) -> (player.isPlaying())).forEachOrdered((player) -> {
                long winning = table.collectPrize(player);
                if (winning != 0) {
                    player.givePrize(winning);
                    System.out.println(player.getName() + " játékos nyert ennyit: " + winning + ". Új egyenlege: " + player.getAccount());
                } else {
                    System.out.println(player.getName() + " játékos nem nyert. Új egyenlege: " + player.getAccount());
                }
            });
        }
        
        System.out.println("\u001B[34m"+"==Vége=="+"\u001B[0m");

        printResults();

        System.out.println("\u001B[34m"+"Meg akarod nézni a számstatisztikákat? (I/N)"+"\u001B[0m");
        String str = "";
        Scanner sc = new Scanner(System.in);
        while (str.equals("")) {
            str = sc.nextLine();
            if (!str.toLowerCase().equals("i") && !str.toLowerCase().equals("n")) {
                System.out.println("I vagy N!");
                str = "";
            }
        }
        if (str.toLowerCase().equals("i")) {
            table.printStats();
        }

    }

    public void doQuickSimulation(int count) {

        RulettTable table = new RulettTable("European");
        this.placeablePositions = table.getPlaceablePositions();
        initPlayers(true);
        System.out.println(this.players.size() + " játékos vesz részt a szimulációban.");

        for (int i = 1; i <= count; i++) {

            boolean zeroPlayer = true;
            for (Player player : players) {
                if (player.isPlaying()) {
                    zeroPlayer = false;
                    break;
                }
            }

            if (zeroPlayer) {
                System.out.println("Kör " + i + ".: A játék véget ért, elfogytak a játékosok.");
                break;
            }

            for (Player player : players) {
                if (player.isPlaying()) {

                    ArrayList<Bet> b = player.getNextBet(table);
                    if (b != null && !b.isEmpty()) {
                        if (b.get(0).getMoney() != 0) {
                            table.placeBet(b);
                        } else {

                        }
                    } else {
                        System.out.println("Kör " + i + ": " + player.getName() + " kiszállt a játékból.");
                    }
                }
            }
            number = table.spin();
            players.stream().filter((player) -> (player.isPlaying())).forEachOrdered((player) -> {
                long winning = table.collectPrize(player);
                if (winning != 0) {
                    player.givePrize(winning);
                } else {
                }
            });
        }

        System.out.println("\u001B[34m"+"==Vége=="+"\u001B[0m");

        printResults();

        
        System.out.println("\u001B[34m"+"Meg akarod nézni a számstatisztikákat? (I/N)"+"\u001B[0m");
        String str = "";
        Scanner sc = new Scanner(System.in);
        while (str.equals("")) {
            str = sc.nextLine();
            if (!str.toLowerCase().equals("i") && !str.toLowerCase().equals("n")) {
                System.out.println("I vagy N!");
                str = "";
            }
        }
        if (str.toLowerCase().equals("i")) {
            table.printStats();
        }
        
        
    }
    
    public void printResults(){
        
        Collections.sort(players, (Player p1, Player p2) -> (
               p1.getPlayedRounds() == p2.getPlayedRounds() ?
                (int)(p2.getAccount() - p1.getAccount()) :
                p2.getPlayedRounds() - p1.getPlayedRounds())
            );
        
        players.stream().map((player) -> {
            System.out.println(player.getName() + ":");
            return player;
        }).map((player) -> {
            System.out.println("\tAktívan lejátszott körök száma: " + player.getPlayedRounds());
            return player;
        }).map((player) -> {
            System.out.println("\tEgyenleg a játék végén: " + player.getAccount());
            return player;
        }).map((player) -> {
            System.out.println("\tLegnagyobb tét a játék során: " + player.getMaxBetInHistory());
            return player;
        }).map((player) -> {
            System.out.println("\tLegmagasabb egyenleg a játék során: " + player.getMaxAccountInHistory());
            return player;
        }).forEachOrdered((player) -> {
            System.out.println("\tLegalacsonyabb egyenleg a játék során (mielőtt esetleg elvesztett mindent): " + player.getMinAccountInHistory());
        });
        
    }
    
    public void addPlayer(Player p){
        int cnt = 1;
        for (Player player : players) {
            if (player.getType().equals(p.getType())){
                cnt++;
            }
        }
        if (!p.getType().equals("Human") && cnt != 1){
            p.setName(p.getName()+cnt);
        }
        players.add(p);
        System.out.println(p.getName()+" hozzáadva a játékosokhoz.");
    }
}
