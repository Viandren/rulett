/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RulettGame;

/**
 *
 * @author robb
 */
public class Bet {
    
    private long money;
    private Position pos;
    private Player owner;
    private boolean checked = false;

    public Bet() {
    }
    
    

    public Bet(long money, Position pos, Player owner) {
        this.money = money;
        this.pos = pos;
        this.owner = owner;
    }

    public long getMoney() {
        return money;
    }

    public void setMoney(long money) {
        this.money = money;
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        this.pos = pos;
    }

    public Player getOwner() {
        return owner;
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    
    
    
}
