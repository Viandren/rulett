/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RulettGame;

import java.util.ArrayList;

/**
 *
 * @author robb
 */

public class Position {
    
    private ArrayList<Integer> numbers = new ArrayList<>();
    private String nameId;
    private ArrayList<Bet> bets = new ArrayList<>();

    public Position() {
    }

    public Position(String nameId) {
        this.nameId = nameId;
    }
    
    

    public Position(ArrayList<Integer> numbers) {
        this.numbers = numbers;
    }

    public Position(ArrayList<Integer> numbers, String nameId) {
        this.numbers = numbers;
        this.nameId = nameId;
    }
    
    

    public ArrayList<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(ArrayList<Integer> numbers) {
        this.numbers = numbers;
    }

    public String getNameId() {
        return nameId;
    }

    public void setNameId(String nameId) {
        this.nameId = nameId;
    }

    
    
    public boolean isWinner(int num){
        return numbers.contains(num);
    }

    public ArrayList<Bet> getBets() {
        return bets;
    }

    public void setBets(ArrayList<Bet> bets) {
        this.bets = bets;
    }
    
    void placeBet(Bet b){
        this.bets.add(b);
    }
    
    void removeBet(Bet b){
        this.bets.remove(b);
    }
    
    public long getPrize(long money){
        return (long)(money*(36/numbers.size()));
    }
    
}
