/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RulettGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author robb
 */
public class RulettTable {

    private final String gameType;
    private final HashMap<String, Position> placeablePositions;
    private int number;
    private final ArrayList<Integer> winnerNumbers = new ArrayList<>();
    private final HashMap<Integer, Integer> numberStats = new HashMap<>();
    private int spinNum = 0;
    private int blackNum = 0;
    private int redNum = 0;
    private int evenNum = 0;
    private int oddNum = 0;
    private int lastBlack = 0;
    private int lastRed = 0;
    private int maxColorInRow = 0;

    public RulettTable(String gameType) {
        this.gameType = gameType;
        placeablePositions = new HashMap<>();
        initTable();
        for (int i = 0; i <= 36; i++) {
            numberStats.putIfAbsent(i, 0);
        }
    }

    public int getSpinNum() {
        return spinNum;
    }

    public int getBlackNum() {
        return blackNum;
    }

    public int getRedNum() {
        return redNum;
    }

    public int getLastBlack() {
        return lastBlack;
    }

    public int getLastRed() {
        return lastRed;
    }

    public int getNumber() {
        return number;
    }

    private void initTable() {

        for (int i = 0; i <= 36; i++) {
            placeablePositions.putIfAbsent(String.valueOf(i), new Position(String.valueOf(i)));
            placeablePositions.get(String.valueOf(i)).getNumbers().add(i);
        }

        placeablePositions.putIfAbsent("Red", new Position("Red"));
        for (int i = 1; i <= 36; i++) {
            placeablePositions.get("Red").getNumbers().add(i);
            if (i == 9 || i == 27) {
                i++;
            }
            if (i != 18) {
                i++;
            }

        }

        placeablePositions.putIfAbsent("Black", new Position("Black"));
        for (int i = 2; i <= 36; i++) {
            placeablePositions.get("Black").getNumbers().add(i);
            if (i == 17) {
                i++;
            }
            if (i != 10 && i != 28) {
                i++;
            }

        }

        placeablePositions.putIfAbsent("First Half", new Position("First Half"));
        for (int i = 1; i <= 18; i++) {
            placeablePositions.get("First Half").getNumbers().add(i);
        }

        placeablePositions.putIfAbsent("Second Half", new Position("Second Half"));
        for (int i = 19; i <= 36; i++) {
            placeablePositions.get("Second Half").getNumbers().add(i);
        }

        placeablePositions.putIfAbsent("First Third", new Position("First Third"));
        for (int i = 1; i <= 12; i++) {
            placeablePositions.get("First Third").getNumbers().add(i);
        }

        placeablePositions.putIfAbsent("Second Third", new Position("Second Third"));
        for (int i = 13; i <= 24; i++) {
            placeablePositions.get("Second Third").getNumbers().add(i);
        }

        placeablePositions.putIfAbsent("Third Third", new Position("Third Third"));
        for (int i = 25; i <= 36; i++) {
            placeablePositions.get("Third Third").getNumbers().add(i);
        }

        placeablePositions.putIfAbsent("Even", new Position("Even"));
        for (int i = 1; i <= 36; i++) {
            if (i % 2 == 0) {
                placeablePositions.get("Even").getNumbers().add(i);
            }
        }

        placeablePositions.putIfAbsent("Odd", new Position("Odd"));
        for (int i = 1; i <= 36; i++) {
            if (i % 2 == 1) {
                placeablePositions.get("Odd").getNumbers().add(i);
            }
        }

        placeablePositions.putIfAbsent("First Row", new Position("First Row"));
        for (int i = 1; i <= 36; i++) {
            placeablePositions.get("First Row").getNumbers().add(i);
            i += 2;
        }
        //System.out.println(placeablePositions.get("First Row").getNumbers().toString());
        placeablePositions.putIfAbsent("Second Row", new Position("Second Row"));
        for (int i = 2; i <= 36; i++) {
            placeablePositions.get("Second Row").getNumbers().add(i);
            i += 2;
        }
        //System.out.println(placeablePositions.get("Second Row").getNumbers().toString());
        placeablePositions.putIfAbsent("Third Row", new Position("Third Row"));
        for (int i = 3; i <= 36; i++) {
            placeablePositions.get("Third Row").getNumbers().add(i);
            i += 2;
        }
        //System.out.println(placeablePositions.get("Third Row").getNumbers().toString());
        for (int i = 1; i <= 12; i++) {
            placeablePositions.putIfAbsent("Coloumn" + i, new Position("Coloumn" + i));
            for (int j = (i - 1) * 3 + 1; j < (i - 1) * 3 + 1 + 3; j++) {
                placeablePositions.get("Coloumn" + i).getNumbers().add(j);
            }
        }
        //System.out.println(placeablePositions.get("Coloumn10").getNumbers().toString());

        for (int i = 1; i <= 36; i++) {
            if (i % 3 == 0) {
                placeablePositions.putIfAbsent("Pair" + i + "-" + (i + 3), new Position("Pair" + i + "-" + (i + 3)));
                placeablePositions.get("Pair" + i + "-" + (i + 3)).getNumbers().add(i);
                placeablePositions.get("Pair" + i + "-" + (i + 3)).getNumbers().add(i + 3);
            } else {
                placeablePositions.putIfAbsent("Pair" + i + "-" + (i + 1), new Position("Pair" + i + "-" + (i + 1)));
                placeablePositions.get("Pair" + i + "-" + (i + 1)).getNumbers().add(i);
                placeablePositions.get("Pair" + i + "-" + (i + 1)).getNumbers().add(i + 1);

                placeablePositions.putIfAbsent("Pair" + i + "-" + (i + 3), new Position("Pair" + i + "-" + (i + 3)));
                placeablePositions.get("Pair" + i + "-" + (i + 3)).getNumbers().add(i);
                placeablePositions.get("Pair" + i + "-" + (i + 3)).getNumbers().add(i + 3);
            }

        }
        //System.out.println(placeablePositions.get("Pair32-33").getNumbers().toString());
        for (int i = 0; i < placeablePositions.get("First Row").getNumbers().size() - 1; i++) {
            int num = placeablePositions.get("First Row").getNumbers().get(i);
            placeablePositions.putIfAbsent("FourFrom" + num, new Position("FourFrom" + num));
            placeablePositions.get("FourFrom" + num).getNumbers().add(num);
            placeablePositions.get("FourFrom" + num).getNumbers().add(num + 1);
            placeablePositions.get("FourFrom" + num).getNumbers().add(num + 3);
            placeablePositions.get("FourFrom" + num).getNumbers().add(num + 4);
        }
        for (int i = 0; i < placeablePositions.get("Second Row").getNumbers().size() - 1; i++) {
            int num = placeablePositions.get("Second Row").getNumbers().get(i);
            placeablePositions.putIfAbsent("FourFrom" + num, new Position("FourFrom" + num));
            placeablePositions.get("FourFrom" + num).getNumbers().add(num);
            placeablePositions.get("FourFrom" + num).getNumbers().add(num + 1);
            placeablePositions.get("FourFrom" + num).getNumbers().add(num + 3);
            placeablePositions.get("FourFrom" + num).getNumbers().add(num + 4);
        }
        placeablePositions.putIfAbsent("FourFrom0", new Position("FourFrom0"));
        placeablePositions.get("FourFrom0").getNumbers().add(0);
        placeablePositions.get("FourFrom0").getNumbers().add(1);
        placeablePositions.get("FourFrom0").getNumbers().add(2);
        placeablePositions.get("FourFrom0").getNumbers().add(3);
        //System.out.println(placeablePositions.get("FourFrom32").getNumbers().toString());
        for (int i = 1; i <= 31; i += 3) {
            placeablePositions.putIfAbsent("SixFrom" + i, new Position("SixFrom" + i));
            for (int j = i; j < i + 6; j++) {
                placeablePositions.get("SixFrom" + i).getNumbers().add(j);
            }
        }
        //System.out.println(placeablePositions.get("SixFrom31").getNumbers().toString());
        /**
         * "number" "Red" "Black" "First Half" "Second Half" "First Third"
         * "Second Third" "Third Third" "Even" "Odd"
         *
         * "PairX-Y" "FourFromX" "SixFromX" "ColoumnX" "First Row" "Second Row"
         * "Third Row"
         *
         */
    }

    public HashMap<String, Position> getPlaceablePositions() {
        return placeablePositions;
    }

    public void placeBet(ArrayList<Bet> bet) {
        bet.forEach((b) -> {
            placeablePositions.get(b.getPos().getNameId()).placeBet(b);
        });

    }

    public long collectPrize(Player p) {
        long res = 0;

        placeablePositions.values().stream().filter((pos) -> (!pos.isWinner(number))).forEachOrdered((pos) -> {
            pos.getBets().clear();
        });

        for (Position pos : placeablePositions.values()) {
            for (Bet bet : pos.getBets()) {
                if (bet.getOwner().equals(p) && !bet.isChecked()) {
                    res += pos.getPrize(bet.getMoney());
                    bet.setChecked(true);
                }
            }
        }
        return res;
    }

    public int spin() {
        number = (int) (Math.random() * 37);
        numberStats.putIfAbsent(number, 0);
        numberStats.put(number, numberStats.get(number) + 1);
        spinNum++;
        if (placeablePositions.get("Black").getNumbers().contains(number)) {
            blackNum++;
            lastBlack = 0;
            lastRed++;
        } else if (number == 0) {
            lastRed++;
            lastBlack++;
        } else {
            redNum++;
            lastRed = 0;
            lastBlack++;
        }
        if (placeablePositions.get("Even").getNumbers().contains(number)) {
            evenNum++;
        } else if (number == 0) {
        } else {
            oddNum++;
        }

        if (lastBlack > maxColorInRow) {
            maxColorInRow = lastBlack;
        }
        if (lastRed > maxColorInRow) {
            maxColorInRow = lastRed;
        }

        return number;
    }

    public void printStats() {
        /*
        Scanner sc = new Scanner(System.in);
         */
        System.out.println("Az összes pörgetés: ");
        System.out.println("\t" + spinNum);
        System.out.println("Az egyes számok gyakorisága: ");
        ArrayList<Map.Entry<Integer, Integer>> list = new ArrayList<>(numberStats.entrySet());
        Collections.sort(list, (Map.Entry<Integer, Integer> e1, Map.Entry<Integer, Integer> e2) -> (e2.getValue() - e1.getValue()));

        list.forEach((entry) -> {
            //if (entry.getValue() != 0) {
                System.out.println("\t" + entry.getKey() + " - " + entry.getValue() + " db (" + (double) ((int) ((entry.getValue() / (double) spinNum) * 10000)) / 100 + " %)");
           // }
        });
        System.out.println("Piros: ");
        System.out.println("\t" + redNum + " (" + (double) ((int) ((redNum / (double) spinNum) * 10000)) / 100 + " %)");
        System.out.println("Fekete: ");
        System.out.println("\t" + blackNum + " (" + (double) ((int) ((blackNum / (double) spinNum) * 10000)) / 100 + " %)");
        System.out.println("Páros számok: ");
        System.out.println("\t" + evenNum + " (" + (double) ((int) ((evenNum / (double) spinNum) * 10000)) / 100 + " %)");
        System.out.println("Páratlan számok: ");
        System.out.println("\t" + oddNum + " (" + (double) ((int) ((oddNum / (double) spinNum) * 10000)) / 100 + " %)");
        System.out.println("Leghosszabb színsorozat:");
        System.out.println("\t" + maxColorInRow);
    }

    public int getRarestNumber() {

        if (numberStats.isEmpty()) {
            return -1;
        }
        ArrayList<Map.Entry<Integer, Integer>> list = new ArrayList<>(numberStats.entrySet());
        Collections.sort(list, (Map.Entry<Integer, Integer> e1, Map.Entry<Integer, Integer> e2) -> (e2.getValue() - e1.getValue()));

        return list.get(list.size() - 1).getKey();
    }

    public Set<Integer> getFiveRarestNumber() {

        Set<Integer> resList = new HashSet<>();

        if (numberStats.isEmpty() || spinNum < 10) {
            while (resList.size() < 5) {
                resList.add((int) (Math.random() * 37));
            }
            return resList;
        }
        ArrayList<Map.Entry<Integer, Integer>> list = new ArrayList<>(numberStats.entrySet());
        Collections.sort(list, (Map.Entry<Integer, Integer> e1, Map.Entry<Integer, Integer> e2) -> (e2.getValue() - e1.getValue()));
        for (int i = 0; i < 5; i++) {
            resList.add(list.get(list.size() - 1 - i).getKey());
        }

        return resList;
    }

    public void printTip() {
        System.out.println("Hasznos tanács:");
        System.out.println("A rulettben hosszútávon nem lehet nyerni.");
        System.out.println("A nulla miatt matematikailag lehetetlen, hogy valaki olyan stratégiával álljon elő,");
        System.out.println("ami hosszútávon nyereséges.");
        System.out.println("(A szorzók nem veszik figyelembe a nullát, úgy viselkednek, mintha csak 36 szám lenne.");
        System.out.println("A maradék 1/37 = 2.7 % mindig a bankot fogja gazdagítani.)");
        System.out.println("A szakirodalom a szerencsejátékos tévedésének nevezi azt a tévhitet, amibe sokan ringatják magukat,");
        System.out.println("mégpedig hogy a múlt statisztikái hatással vannak a számok előfordulására a jövőben.");
        System.out.println("Ez egyáltalán nincs így. Bármilyen hosszú sorozatot látunk például a fekete színből,");
        System.out.println("ugyanakkora marad az esélye annak, hogy újra a fekete szín jelenik meg.");
        System.out.println("Mindennek csak látszólag mond ellent, hogy a statisztikai értékek hosszútávon");
        System.out.println("mindig a saját valószínűségük felé közelítenek.");
        System.out.println("A szimulációk alapján is megállapítható, hogy idővel a progresszív stratégiák");
        System.out.println("csúnyán csődőt mondanak. Ez független attól, hogy mekkora a kaszinó által maximalizált tét.");
        System.out.println("A Martingale stratégiával például hamarabb futunk bele olyan sorozatba, ami x összeg befektetését");
        System.out.println("teszi szükségessé, mint hogy megnyernénk azt az x összeget. Így idővel mindig mínuszba kerülünk.");
        System.out.println("Matematikailag modellezhető nyerő stratégiát csak akkor lehetne létrehozni, ha bevezetnénk");
        System.out.println("a negatív tét lehetőségét. Összegezve: minél alacsonyabbak a tétjeid, annál tovább húzhatod az asztal mellett anélkül,");
        System.out.println("hogy mindened elveszítenéd.");

    }
}
