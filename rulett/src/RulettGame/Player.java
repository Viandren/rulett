/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RulettGame;

import MenuApi.Menu;
import MenuApi.MenuListener;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author robb
 */
public class Player {

    private final long minimumBet;
    private final long maximumBet;
    private long account;
    private final String type;
    private ArrayList<Bet> historyOfBets;
    private ArrayList<ArrayList<Bet>> historyOfBetLists;
    private ArrayList<Long> historyOfAccount;
    private ArrayList<Boolean> historyOfWinnings;
    private boolean win = false;
    private boolean playing = true;

    private long maxBetInHistory = 0;
    private long minAccountInHistory;
    private long maxAccountInHistory = 0;
    private int playedRounds = 0;

    private long lastAcc = 0;
    private int seqNum = 0;
    private long seqBet;

    private String name = "";

    private int seq1326 = 1;

    private static int idCounter = 0;
    private int id;
    private boolean enough;

    public Player(long minimumBet, long maximumBet, long account, String type) {
        this.minimumBet = minimumBet;
        this.maximumBet = maximumBet;
        this.account = account;
        this.type = type;
        historyOfBets = new ArrayList<>();
        historyOfBetLists = new ArrayList<>();
        historyOfAccount = new ArrayList<>();
        historyOfWinnings = new ArrayList<>();
        minAccountInHistory = account;
        lastAcc = account;
        this.name = type;

        id = idCounter;
        idCounter++;
    }

    public Player(String name, long minimumBet, long maximumBet, long account, String type) {
        this.minimumBet = minimumBet;
        this.maximumBet = maximumBet;
        this.account = account;
        this.type = type;
        historyOfBets = new ArrayList<>();
        historyOfBetLists = new ArrayList<>();
        historyOfAccount = new ArrayList<>();
        historyOfWinnings = new ArrayList<>();
        minAccountInHistory = account;
        lastAcc = account;
        this.name = name;

        id = idCounter;
        idCounter++;
    }

    public long getAccount() {
        return account;
    }

    public void setAccount(long account) {
        this.account = account;
    }

    public ArrayList<Bet> getHistoryOfBets() {
        return historyOfBets;
    }

    public void setHistoryOfBets(ArrayList<Bet> historyOfBets) {
        this.historyOfBets = historyOfBets;
    }

    public ArrayList<Long> getHistoryOfAccount() {
        return historyOfAccount;
    }

    public void setHistoryOfAccount(ArrayList<Long> historyOfAccount) {
        this.historyOfAccount = historyOfAccount;
    }

    public ArrayList<Boolean> getHistoryOfWinnings() {
        return historyOfWinnings;
    }

    public void setHistoryOfWinnings(ArrayList<Boolean> historyOfWinnings) {
        this.historyOfWinnings = historyOfWinnings;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public long getMaxBetInHistory() {
        return maxBetInHistory;
    }

    public void setMaxBetInHistory(long maxBetInHistory) {
        this.maxBetInHistory = maxBetInHistory;
    }

    public long getMinAccountInHistory() {
        return minAccountInHistory;
    }

    public void setMinAccountInHistory(long minAccountInHistory) {
        this.minAccountInHistory = minAccountInHistory;
    }

    public long getMaxAccountInHistory() {
        return maxAccountInHistory;
    }

    public int getPlayedRounds() {
        return playedRounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public ArrayList<Bet> getNextBet(RulettTable table) {
        if (account == 0) {
            playing = false;
            return null;
        }
        ArrayList<Bet> resList = new ArrayList<>();
        Bet res = new Bet();
        res.setMoney(-1);
        res.setOwner(this);

        if (account < minAccountInHistory) {
            minAccountInHistory = account;
        }
        if (account > maxAccountInHistory) {
            maxAccountInHistory = account;
        }

        if (type.equals("Martin")) {
            res.setPos(table.getPlaceablePositions().get("Black"));
            if (win) {
                res.setMoney(minimumBet);
            } else {
                if (!historyOfBets.isEmpty()) {
                    res.setMoney(historyOfBets.get(historyOfBets.size() - 1).getMoney() * 2);
                } else {
                    res.setMoney(minimumBet);
                }
            }
            if (res.getMoney() > maximumBet) {
                res.setMoney(maximumBet);
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
            }
        }

        if (type.equals("CareFulMartin")) {
            res.setPos(table.getPlaceablePositions().get("Red"));
            if (win) {
                res.setMoney(minimumBet);
            } else {
                if (!historyOfBets.isEmpty()) {
                    res.setMoney(historyOfBets.get(historyOfBets.size() - 1).getMoney() * 2);
                } else {
                    res.setMoney(minimumBet);
                }
            }
            if (res.getMoney() > minimumBet * 500) {
                res.setMoney(minimumBet);
            }

            if (res.getMoney() > maximumBet) {
                res.setMoney(maximumBet);
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
            }
        }

        if (type.equals("AgressiveMartin")) {
            res.setPos(table.getPlaceablePositions().get("Red"));
            if (win) {
                res.setMoney(minimumBet);
            } else {
                if (!historyOfBets.isEmpty()) {
                    res.setMoney(historyOfBets.get(historyOfBets.size() - 1).getMoney() * 3);
                } else {
                    res.setMoney(minimumBet);
                }
            }

            if (res.getMoney() > maximumBet) {
                res.setMoney(maximumBet);
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
            }
        }

        if (type.equals("RandomBéla")) {
            ArrayList<Position> poslist = new ArrayList<>(table.getPlaceablePositions().values());
            int r = (int) (Math.random() * poslist.size());
            res.setPos(poslist.get(r));
            long money = (long) (Math.random() * 100);
            res.setMoney(money);
            if (res.getMoney() > account) {
                res.setMoney(account);
            }
        }

        if (type.equals("Human")) {
            enough = false;

            while (!enough) {

                Menu main = new Menu(name + ", válassz tétet:", (String menuName, int id2) -> {
                    if (menuName.equals("Piros")) {
                        res.setPos(table.getPlaceablePositions().get("Red"));
                    }
                    if (menuName.equals("Fekete")) {
                        res.setPos(table.getPlaceablePositions().get("Black"));
                    }
                    if (menuName.equals("Szám")) {
                        System.out.println("Melyik szám legyen?");
                        String numstr = "";
                        Scanner sc = new Scanner(System.in);
                        while (numstr.equals("")) {
                            numstr = sc.nextLine();
                            if (table.getPlaceablePositions().get(numstr) != null && table.getPlaceablePositions().get(numstr).getNumbers().size() == 1) {
                                System.out.println("A következő számot adtad meg: " + numstr);
                                res.setPos(table.getPlaceablePositions().get(numstr));
                            } else {
                                System.out.println("Ez nem jó szám! Nincs ilyen opció...");
                                numstr = "";
                            }
                        }
                    }
                    if (menuName.equals("Oszlop")) {
                        System.out.println("Melyik oszlop legyen?");
                        String numstr = "";
                        Scanner sc = new Scanner(System.in);
                        while (numstr.equals("")) {
                            numstr = sc.nextLine();
                            if (table.getPlaceablePositions().get("Coloumn" + numstr) != null) {
                                System.out.println("A következő számot adtad meg: " + numstr);
                                res.setPos(table.getPlaceablePositions().get("Coloumn" + numstr));
                            } else {
                                System.out.println("Ez nem jó szám! Nincs ilyen opció...");
                                numstr = "";
                            }
                        }
                    }
                    if (menuName.equals("Első harmad")) {
                        res.setPos(table.getPlaceablePositions().get("First Third"));
                    }
                    if (menuName.equals("Második harmad")) {
                        res.setPos(table.getPlaceablePositions().get("Second Third"));
                    }
                    if (menuName.equals("Harmadik harmad")) {
                        res.setPos(table.getPlaceablePositions().get("Third Third"));
                    }
                    if (menuName.equals("Első fél")) {
                        res.setPos(table.getPlaceablePositions().get("First Half"));
                    }
                    if (menuName.equals("Második fél")) {
                        res.setPos(table.getPlaceablePositions().get("Second Half"));
                    }
                    if (menuName.equals("Páros")) {
                        res.setPos(table.getPlaceablePositions().get("Even"));
                    }
                    if (menuName.equals("Páratlan")) {
                        res.setPos(table.getPlaceablePositions().get("Odd"));
                    }
                    if (menuName.equals("Első sor")) {
                        res.setPos(table.getPlaceablePositions().get("First Row"));
                    }
                    if (menuName.equals("Második sor")) {
                        res.setPos(table.getPlaceablePositions().get("Second Row"));
                    }
                    if (menuName.equals("Harmadik sor")) {
                        res.setPos(table.getPlaceablePositions().get("Third Row"));
                    }
                    if (menuName.equals("Számpár")) {
                        boolean megvan = false;
                        while (!megvan) {
                            int num1 = 0;
                            int num2 = 0;
                            System.out.println("Add meg az első számot!");
                            Scanner sc = new Scanner(System.in);
                            String str = "";
                            while (str.equals("")) {
                                str = sc.nextLine();
                                int tet = -1;
                                try {
                                    tet = Integer.parseInt(str);
                                } catch (NumberFormatException e) {
                                    str = "";
                                    System.out.println("Számot adj meg, légyszíves!");
                                }
                                if (tet < 36 && tet > -1) {
                                    num1 = tet;
                                } else {
                                    System.out.println("Ez nem megfelelő szám!");
                                    str = "";
                                }
                            }
                            System.out.println("Add meg a második számot!");
                            str = "";
                            while (str.equals("")) {
                                str = sc.nextLine();
                                int tet = -1;
                                try {
                                    tet = Integer.parseInt(str);
                                } catch (NumberFormatException e) {
                                    str = "";
                                    System.out.println("Számot adj meg, légyszíves!");
                                }
                                if (tet < 37 && tet > -1) {
                                    num2 = tet;
                                } else {
                                    System.out.println("Ez nem megfelelő szám!");
                                    str = "";
                                }
                            }

                            if (table.getPlaceablePositions().get("Pair" + num1 + "-" + num2) != null) {
                                megvan = true;
                                res.setPos(table.getPlaceablePositions().get("Pair" + num1 + "-" + num2));
                            } else {
                                System.out.println("Nincs ilyen számpáros! Próbáld meg újra!");
                            }

                        }
                    }
                    if (menuName.equals("Számnégyes")) {
                        boolean megvan = false;
                        while (!megvan) {
                            int num = 0;
                            System.out.println("Add meg a számnégyes első (legkisebb) számát!");
                            Scanner sc = new Scanner(System.in);
                            String str = "";
                            while (str.equals("")) {
                                str = sc.nextLine();
                                int tet = -1;
                                try {
                                    tet = Integer.parseInt(str);
                                } catch (NumberFormatException e) {
                                    str = "";
                                    System.out.println("Számot adj meg, légyszíves!");
                                }
                                if (tet < 33 && tet > -1) {
                                    num = tet;
                                } else {
                                    System.out.println("Ez nem megfelelő szám!");
                                    str = "";
                                }
                            }
                            if (table.getPlaceablePositions().get("FourFrom" + num) != null) {
                                megvan = true;
                                res.setPos(table.getPlaceablePositions().get("FourFrom" + num));
                            } else {
                                System.out.println("Nincs ilyen számnégyes! Próbáld meg újra!");
                            }
                        }
                    }

                    if (menuName.equals("Számhatos")) {
                        boolean megvan = false;
                        while (!megvan) {
                            int num = 0;
                            System.out.println("Add meg a számhatos első (legkisebb) számát!");
                            Scanner sc = new Scanner(System.in);
                            String str = "";
                            while (str.equals("")) {
                                str = sc.nextLine();
                                int tet = -1;
                                try {
                                    tet = Integer.parseInt(str);
                                } catch (NumberFormatException e) {
                                    str = "";
                                    System.out.println("Számot adj meg, légyszíves!");
                                }
                                if (tet < 32 && tet > -1) {
                                    num = tet;
                                } else {
                                    System.out.println("Ez nem megfelelő szám!");
                                    str = "";
                                }
                            }
                            if (table.getPlaceablePositions().get("SixFrom" + num) != null) {
                                megvan = true;
                                res.setPos(table.getPlaceablePositions().get("SixFrom" + num));
                            } else {
                                System.out.println("Nincs ilyen számhatos! Próbáld meg újra!");
                            }
                        }
                    }

                    if (menuName.equals("Kilépés, vége a játéknak")) {
                        System.out.println("Köszi a játékot!");
                        res.setMoney(0);
                        playing = false;

                        //System.exit(0);
                    }
                    if (menuName.equals("Az eddigi statisztikák megjelenítése")) {
                        table.printStats();
                        //res.clone(getNextBet(table));
                        return;
                    }
                    if (menuName.equals("Tipp")) {
                        table.printTip();
                        //res.clone(getNextBet(table));
                        return;
                    }
                    if (menuName.equals("Kimaradok ebből a körből")) {
                        res.setMoney(0);
                        return;
                    }

                    if (playing) {
                        System.out.println("Mekkora összeget szeretnél feltenni?");
                        Scanner sc = new Scanner(System.in);
                        String str = "";
                        while (str.equals("")) {
                            str = sc.nextLine();
                            int tet = (int) minimumBet;
                            try {
                                tet = Integer.parseInt(str);
                            } catch (NumberFormatException e) {
                                str = "";
                                System.out.println("Számot adj meg, légyszíves!");
                            }
                            if (!(tet < account) || !(tet < maximumBet) || !(tet > minimumBet)) {
                                System.out.println("Nem jó összeg. Adj meg egy másikat.");
                                System.out.println("Az egyenleged: " + account + ". A minimumtét: " + minimumBet + ". A maximumtét: " + maximumBet + ".");
                                str = "";
                            } else {
                                res.setMoney(tet);
                            }
                        }
                    }
                });
                Menu s0 = new Menu("Kimaradok ebből a körből", main);
                Menu s1 = new Menu("Szám", main);
                Menu s2 = new Menu("Szín", main);
                Menu s3 = new Menu("Piros", s2);
                Menu s4 = new Menu("Fekete", s2);
                Menu s5 = new Menu("Harmad", main);
                Menu s6 = new Menu("Első harmad", s5);
                Menu s7 = new Menu("Második harmad", s5);
                Menu s8 = new Menu("Harmadik harmad", s5);
                Menu s9 = new Menu("Sor", main);
                Menu s91 = new Menu("Első sor", s9);
                Menu s92 = new Menu("Második sor", s9);
                Menu s93 = new Menu("Harmadik sor", s9);
                Menu s10 = new Menu("Oszlop", main);
                Menu s11 = new Menu("Számpár", main);
                Menu s12 = new Menu("Számnégyes", main);
                Menu s13 = new Menu("Számhatos", main);
                Menu s14 = new Menu("Páros/Páratlan", main);
                Menu s15 = new Menu("Páros", s14);
                Menu s16 = new Menu("Páratlan", s14);
                Menu s17 = new Menu("Fél terület", main);
                Menu s18 = new Menu("Első fél", s17);
                Menu s19 = new Menu("Második fél", s17);
                Menu s20 = new Menu("Az eddigi statisztikák megjelenítése", main);
                Menu s22 = new Menu("Tipp", main);
                Menu s21 = new Menu("Kilépés, vége a játéknak", main);
                main.setReturnAfterAction(true);

                Menu s30 = new Menu("Akarsz még tétet tenni?", new MenuListener() {
                    @Override
                    public void executeMenuAction(String menuName, int id) {
                        if (menuName.equals("Nem")) {
                            enough = true;
                        }
                        if (menuName.equals("Igen")) {
                            resList.add(new Bet(res.getMoney(), res.getPos(), res.getOwner()));
                            account -= res.getMoney();
                            res.setMoney(-1);
                        }
                    }
                });
                Menu s31 = new Menu("Igen", s30);
                Menu s32 = new Menu("Nem", s30);

                s30.setReturnAfterAction(true);
                s30.setColor(Menu.Color.BLUE);
                main.setColorAllDown(Menu.Color.BLUE);

                while (res.getMoney() == -1) {
                    main.displayMenu();
                }

                if (res.getMoney() != 0) {
                    s30.displayMenu();
                } else {
                    enough = true;
                }
                //res.setPos(placeablePositions.get("Red"));

            }
        }

        if (type.equals("RareNumberGéza")) {
            if (table.getSpinNum() >= 100) {
                int num = table.getRarestNumber();
                if (num != -1) {
                    res.setPos(table.getPlaceablePositions().get(String.valueOf(num)));
                    res.setMoney(minimumBet * 2);
                } else {
                    res.setMoney(0);
                }
            } else {
                res.setMoney(0);
            }
        }
        if (type.equals("RarestColorZoltán")) {
            if (table.getSpinNum() > 0) {
                if (table.getBlackNum() >= table.getRedNum()) {
                    res.setPos(table.getPlaceablePositions().get("Red"));
                } else {
                    res.setPos(table.getPlaceablePositions().get("Black"));
                }
                if (win) {
                    res.setMoney(minimumBet);
                } else {
                    if (historyOfBets.size() > 1) {
                        res.setMoney(historyOfBets.get(historyOfBets.size() - 1).getMoney() * 2);
                    } else {
                        res.setMoney(minimumBet);
                    }
                }

            } else {
                res.setMoney(0);
            }
            if (res.getMoney() > maximumBet) {
                res.setMoney(maximumBet);
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
            }

        }

        if (type.equals("PatientMartin")) {
            res.setMoney(0);
            if (table.getLastBlack() >= 5) {
                res.setPos(table.getPlaceablePositions().get("Black"));
                res.setMoney(minimumBet);
            } else if (table.getLastRed() >= 5) {
                res.setPos(table.getPlaceablePositions().get("Red"));
                res.setMoney(minimumBet);
            } else {
                //resList.clear();
                //return resList;
            }

            if (res.getPos() != null && !historyOfBets.isEmpty() && historyOfBets.get(historyOfBets.size() - 1).getMoney() != 0) {
                if (!win) {
                    res.setMoney(historyOfBets.get(historyOfBets.size() - 1).getMoney() * 2);
                } else {
                    res.setMoney(0);
                }
            }
            if (res.getMoney() > maximumBet) {
                res.setMoney(maximumBet);
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
            }
        }

        if (type.equals("ProgressiveHarry")) {
            res.setPos(table.getPlaceablePositions().get("Black"));
            if (seqNum % 25 == 0) {
                seqNum = 1;
                if (lastAcc > account) {
                    seqBet *= 2;
                } else {
                    seqBet = minimumBet;

                }
                lastAcc = account;
            } else {
                seqNum++;
            }

            res.setMoney(seqBet);
            if (res.getMoney() > maximumBet / 2) {
                res.setMoney(minimumBet);
                seqBet = minimumBet;
                lastAcc = account;
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
                seqBet = account;
                lastAcc = account;
            }

        }

        if (type.equals("Fibonacci")) {
            res.setPos(table.getPlaceablePositions().get("Red"));
            long tet = minimumBet;
            if (!historyOfBets.isEmpty()) {
                if (win) {
                    tet = getLastFibNum(getLastFibNum(historyOfBets.get(historyOfBets.size() - 1).getMoney()));
                } else {
                    tet = getNextFibNum(historyOfBets.get(historyOfBets.size() - 1).getMoney());
                }
            }
            res.setMoney(tet);

            if (res.getMoney() > maximumBet) {
                res.setMoney(maximumBet);
            }
            if (res.getMoney() > account) {
                res.setMoney(account);
            }
            if (res.getMoney() < minimumBet) {
                res.setMoney(minimumBet);
            }
        }

        if (type.equals("1326Karcsi")) {
            res.setPos(table.getPlaceablePositions().get("Black"));
            if (win) {
                switch (seq1326) {
                    case 1:
                        res.setMoney(minimumBet * 3);
                        seq1326 = 3;
                        break;
                    case 3:
                        res.setMoney(minimumBet * 1);
                        seq1326 = 2;
                        break;
                    case 2:
                        res.setMoney(minimumBet * 6);
                        seq1326 = 6;
                        break;
                    case 6:
                        res.setMoney(minimumBet * 1);
                        seq1326 = 1;
                }
            } else {
                if (!historyOfBets.isEmpty()) {
                    res.setMoney(historyOfBets.get(historyOfBets.size() - 1).getMoney());
                    seq1326 = 1;
                } else {
                    res.setMoney(minimumBet);
                }
            }
        }

        if (type.equals("FiveRarest")) {
            ArrayList<Integer> numList = new ArrayList<>(table.getFiveRarestNumber());
            numList.forEach((num) -> {
                if (account >= minimumBet){
                resList.add(new Bet(minimumBet, table.getPlaceablePositions().get(String.valueOf(num)), this));
                account -= minimumBet;
                }
            });
            playedRounds++;
            return resList;
        }

        if (type.equals("MultiMartin")) {

            resList.add(new Bet(minimumBet, table.getPlaceablePositions().get("Red"), this));
            resList.add(new Bet(minimumBet, table.getPlaceablePositions().get("Black"), this));

            if (table.getPlaceablePositions().get("Red").getNumbers().contains(table.getNumber())) {
                if (!historyOfBetLists.isEmpty()) {
                    long betMoney = historyOfBetLists.get(historyOfBetLists.size() - 1).get(1).getMoney() * 2;
                    resList.get(1).setMoney(betMoney);
                    resList.get(0).setMoney(minimumBet);
                }
            } else if (table.getPlaceablePositions().get("Black").getNumbers().contains(table.getNumber())) {
                if (!historyOfBetLists.isEmpty()) {
                    long betMoney = historyOfBetLists.get(historyOfBetLists.size() - 1).get(0).getMoney() * 2;
                    resList.get(0).setMoney(betMoney);
                    resList.get(1).setMoney(minimumBet);
                }
            } else {
                if (!historyOfBetLists.isEmpty()) {
                    long betMoney = historyOfBetLists.get(historyOfBetLists.size() - 1).get(1).getMoney() * 2;
                    resList.get(1).setMoney(betMoney);
                    betMoney = historyOfBetLists.get(historyOfBetLists.size() - 1).get(0).getMoney() * 2;
                    resList.get(0).setMoney(betMoney);
                }
            }

            if (resList.get(0).getMoney() > maximumBet) {
                resList.get(0).setMoney(maximumBet);
            }
            if (resList.get(0).getMoney() > account) {
                resList.get(0).setMoney(account);
            }
            account -= resList.get(0).getMoney();
            
            if (resList.get(1).getMoney() > maximumBet) {
                resList.get(1).setMoney(maximumBet);
            }
            if (resList.get(1).getMoney() > account) {
                resList.get(1).setMoney(account);
            }
            
            account -= resList.get(1).getMoney();

            playedRounds++;
            historyOfBetLists.add(resList);
            return resList;
        }

        if (res.getMoney() > maxBetInHistory) {
            maxBetInHistory = res.getMoney();
        }
        if (res.getMoney() != 0) {
            playedRounds++;
        }
        historyOfBets.add(res);
        this.account -= res.getMoney();
        historyOfWinnings.add(win);
        win = false;

        if (!playing) {
            return null;
        }

        resList.add(res);

        return resList;
    }

    public void givePrize(long money) {
        this.account += money;
        win = true;
    }

    public static void printPlayerInfos() {
        System.out.println("Martin:");
        System.out.println("\tA Martingale szisztémát használja.");
        System.out.println("\tHa veszít, megduplázza a tétet, ha nyer, az alaptétet teszi meg.");
        System.out.println("CareFulMartin:");
        System.out.println("\tA Martingale szisztémát használja, de csak a maximumtét");
        System.out.println("\tfeléig megy el. Ha azt elérte, lenyeli a veszteséget,");
        System.out.println("\tés elölről kezdi a minimumtéttől.");
        System.out.println("AgressiveMartin:");
        System.out.println("\tŐ is a Martingale szisztémát használja.");
        System.out.println("\tAzonban ha veszít, triplázza a tétet.");
        System.out.println("\tÁltalában hamar kiesik, de ha szerencséje van, sokat nyer.");
        System.out.println("RandomBéla");
        System.out.println("\tRandom tesz téteket az asztal különböző pontjaira.");
        System.out.println("\tNem valami hatékony stratéga.");
        System.out.println("RareNumberGéza");
        System.out.println("\tAz addigi legritkábban kipörgetett számra tesz alaptétet.");
        System.out.println("\tA századik pörgetés után kezd játszani, hogy legyen ");
        System.out.println("\tstatisztika, amiből kiindulhat.");
        System.out.println("RarestColorZoltán");
        System.out.println("\tFeketére vagy pirosra tesz, attól függően,");
        System.out.println("\tmelyik fordult elő addig ritkábban.");
        System.out.println("PatientMartin");
        System.out.println("\tVár, amíg elérkezik egy ötös sorozat valamelyik színből,");
        System.out.println("\taztán a másikra tesz progresszív tétet.");
        System.out.println("ProgressiveHarry");
        System.out.println("\t25-ös sorozatokban gondolkodik.");
        System.out.println("\tMegteszi 25-ször ugyanazt a tétet, aztán ha veszített, dupláz.");
        System.out.println("Fibonacci");
        System.out.println("\tA Fibonacci sorozat alapján teszi meg a tétjeit.");
        System.out.println("\tHa veszít, előrelép, ha nyer, kettőt vissza.");
        System.out.println("1326Karcsi");
        System.out.println("\tAz 1-3-2-6 rendszert használja.");
        System.out.println("\tHa nyer, a tét a sor aktuális pontjának megfelelően többszöröződik.");
        System.out.println("\tHa veszít, nem változtat a téten.");
        System.out.println("FiveRarest");
        System.out.println("\tMindig az öt legritkábban kipörgetett számra tesz.");
        System.out.println("MultiMartin");
        System.out.println("\tPárhuzamosan tesz a pirosra és a feketére.");
    }

    public long getNextFibNum(long n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 2;
        }
        int cnt = 1;
        long actual = 1;
        long pre1 = 0;
        long pre2;
        if (n != 0) {
            while (actual <= n) {
                pre2 = pre1;
                pre1 = actual;
                actual = pre1 + pre2;
                cnt++;
            }
        } else {
            return 0;
        }
        return actual;
    }

    public long getLastFibNum(long n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 1;
        }
        int cnt = 1;
        long actual = 1;
        long pre1 = 0;
        long pre2;
        if (n != 0) {
            while (actual < n) {
                pre2 = pre1;
                pre1 = actual;
                actual = pre1 + pre2;
                cnt++;
            }
        } else {
            return 0;
        }
        return pre1;
    }

}
